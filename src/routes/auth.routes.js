const { Router } = require('express');
const router = Router();

const { sendSignUpMailService } = require('../services/signUp-mail.service');

router.post('/sendsignupmail', sendSignUpMailService);

module.exports = router;
